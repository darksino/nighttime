#
# Copyright © 2022 Maël A  < mael dot a at tutanota dot com >
#
#
# This file is part of Nighttime.
#
# Nighttime is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Nighttime is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Nighttime. If not, see <https://www.gnu.org/licenses/>.
#

pkgname=nighttime
pkgver=3.1.0
pkgrel=1
pkgdesc="An application to remind you to go to sleep."
url="https://gitlab.com/darksino/nighttime"
license=('GPL3')
source=("https://gitlab.com/darksino/$pkgname/-/archive/v$pkgver/nighttime-v$pkgver.zip")
sha256sums=('e27f8ebdfa61848c11a12e126d8922d38b986a11354be05d68e44bb547f9b120')
sha384sums=('f870470f527cbfa8f4561f4413a22e9041d8e35a1426171b6bd78e768b44753fe1d544c32a3c344d386f67294708de6a')
sha512sums=('bda3c5d884d0df72fd4f0628ac14daf9746c42d6cd5d117534299609a861a99d7ef5df5d199de3aad61eca97060088d8fd56ab92d241db35db0b7bc7553097f9')
b2sums=('bbffbac708e0f15d9592478d872312ed5460920168fec2e13e4912a72a9ea5c3451cdaf7b60f99c295159e910a40ef0582fb37713dd58236f45206028778eb33')
arch=('x86_64')
depends=('libnotify' 'libxdg-basedir' 'hicolor-icon-theme')

build() {
	cd "$pkgname-v$pkgver/src"
	make
}

package() {
	install -D -t "$pkgdir/usr/bin/" "$srcdir/$pkgname-v$pkgver/src/$pkgname"
	install -D -m 644 -t "$pkgdir/usr/lib/systemd/user/" "$srcdir/$pkgname-v$pkgver/$pkgname.service"
	install -D -m 644 -t "$pkgdir/usr/share/doc/$pkgname/" "$srcdir/$pkgname-v$pkgver/README" "$srcdir/$pkgname-v$pkgver/VERSION" "$srcdir/$pkgname-v$pkgver/nighttime.conf.sample"

	cd "$srcdir/$pkgname-v$pkgver/icons/"
	install -D -m 644 -t "$pkgdir/usr/share/icons/hicolor/scalable/status/" *.svg
	install -m 644 -t "$pkgdir/usr/share/doc/$pkgname/" *.LICENSE
}
