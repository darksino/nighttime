/*
 * Copyright © 2022 Maël A  < mael dot a at tutanota dot com >
 *
 *
 * This file is part of Nighttime.
 *
 * Nighttime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nighttime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nighttime. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STRINGS_H
#define STRINGS_H

#define APP_NAME "Sleep Reminder"
#define REMIND_MSG "%u min left before going to bed"
#define SLEEPTIME_MSG "It's time to go to bed !"
#define REMAINING_MSG "Only %u h %u min of sleep left !"

#define OOPS_MSG_SUMMARY "Oops, something went wrong !"
#define OOPS_MSG_BODY APP_NAME " will exit."

#endif /* STRINGS_H */
