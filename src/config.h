/*
 * Copyright © 2022 Maël A  < mael dot a at tutanota dot com >
 *
 *
 * This file is part of Nighttime.
 *
 * Nighttime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nighttime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nighttime. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <stdbool.h>
#include <stddef.h>


struct config {
	unsigned wake_hour;
	unsigned wake_min;
	unsigned weekend_grace; /* in min */

	unsigned sleep_hour;
	unsigned sleep_min;
	unsigned sleep_preparation; /* in min */

	size_t remind_count;
	unsigned *reminds; /* in min */
	size_t warning_count;
	unsigned *warnings; /* in min */
	unsigned warning_repeat; /* in min */
};

extern struct config config;

bool load_config(void);
void unload_config(void);
void print_config(void);

#endif /* CONFIG_H */
