/*
 * Copyright © 2022 Maël A  < mael dot a at tutanota dot com >
 *
 *
 * This file is part of Nighttime.
 *
 * Nighttime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nighttime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nighttime. If not, see <https://www.gnu.org/licenses/>.
 */

#include "notify.h"

#include <libnotify/notify.h>

#include "strings.h"

void notify_time_left(unsigned min_left)
{
	/*
	 * We need space for a 2-digit number but REMIND_MSG already
	 * includes a conversion specification which is always at least two
	 * characters long.
	 */
	size_t summary_len = strlen(REMIND_MSG) + 1;
	char summary[summary_len];
	summary[0] = 0;

	int ret = snprintf(summary, summary_len, REMIND_MSG, min_left);
	if (ret >= (int) summary_len || ret < 0)
		fputs("Warning: notify_time_left(): snprintf()\n", stderr);

	NotifyNotification *notif = notify_notification_new(
					summary, NULL, "nighttime-remind");
	notify_notification_set_timeout(notif, NOTIFY_EXPIRES_DEFAULT);
	/* FIXME: set hint for .desktop file ? */

	GError *error = NULL;
	if (!notify_notification_show(notif, &error)) {
		fprintf(stderr, "Error: notify_notification_show(): %s\n",
								error->message);
		g_clear_error(&error);
	}
}

void notify_sleeptime(void)
{
	NotifyNotification *notif = notify_notification_new(
				SLEEPTIME_MSG, NULL, "nighttime-notify");
	notify_notification_set_timeout(notif, NOTIFY_EXPIRES_DEFAULT);
	/* FIXME: set hint for .desktop file ? */

	GError *error = NULL;
	if (!notify_notification_show(notif, &error)) {
		fprintf(stderr, "Error: notify_notification_show(): %s\n",
								error->message);
		g_clear_error(&error);
	}
}

void notify_sleep_left(unsigned hour, unsigned min)
{
	/*
	 * We need space to format the time (hh:mm) but the conversion
	 * specifications in REMAINING_MSG already provides the
	 * necessary space.
	 */
	size_t summary_len = strlen(REMAINING_MSG) + 1;
	char summary[summary_len];
	summary[0] = 0;

	int ret = snprintf(summary, summary_len, REMAINING_MSG,
								hour, min);
	if (ret >= (int) summary_len || ret < 0)
		fputs("Warning: notify_sleep_left(): snprintf()\n", stderr);

	NotifyNotification *notif = notify_notification_new(
					summary, NULL, "nighttime-warn");
	notify_notification_set_timeout(notif, NOTIFY_EXPIRES_DEFAULT);
	/* FIXME: set hint for .desktop file ? */

	GError *error = NULL;
	if (!notify_notification_show(notif, &error)) {
		fprintf(stderr, "Error: notify_notification_show(): %s\n",
								error->message);
		g_clear_error(&error);
	}
}

void notify_error(void)
{
	NotifyNotification *notif = notify_notification_new(
			OOPS_MSG_SUMMARY, OOPS_MSG_BODY, "nighttime-error");
	notify_notification_set_timeout(notif, NOTIFY_EXPIRES_DEFAULT);
	/* FIXME: set hint for .desktop file ? */

	GError *error = NULL;
	if (!notify_notification_show(notif, &error)) {
		fprintf(stderr, "Error: notify_notification_show(): %s\n",
								error->message);
		g_clear_error(&error);
	}
}
