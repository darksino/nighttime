/*
 * Copyright © 2022 Maël A  < mael dot a at tutanota dot com >
 *
 *
 * This file is part of Nighttime.
 *
 * Nighttime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nighttime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nighttime. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NOTIFY_H
#define NOTIFY_H

void notify_time_left(unsigned min_left);
void notify_sleeptime(void);
void notify_sleep_left(unsigned hour, unsigned min);

void notify_error(void);

#endif /* NOTIFY_H */
