/*
 * Copyright © 2022 Maël A  < mael dot a at tutanota dot com >
 *
 *
 * This file is part of Nighttime.
 *
 * Nighttime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nighttime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nighttime. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <signal.h>

#include <libnotify/notify.h>

#include "strings.h"
#include "config.h"
#include "notify.h"


enum phase {
	REMIND_PHASE,
	NOTIFY_PHASE,
	WARN_PHASE,
	WARN_REPEAT_PHASE
};

struct state {
	enum phase phase;
	size_t index;
	time_t wake_time;
	time_t sleep_time;
	time_t timer_time;
};

static bool exit_program = false;
static sigset_t timer_sigset;
static timer_t timer;
static struct state state;

static void next_remind_state(void)
{
	if (state.index < config.remind_count - 1) {
		state.index++;
		return;
	}

	state.phase = NOTIFY_PHASE;
	state.index = 0;
}

static void next_notify_state(void)
{
	state.phase = WARN_PHASE;
	state.index = 0;
}

static void next_warn_state(void)
{
	if (state.index < config.warning_count - 1) {
		state.index++;
		return;
	}

	state.phase = WARN_REPEAT_PHASE;
	state.index = 0;
}

static void next_state(void)
{
	switch (state.phase) {
	case REMIND_PHASE:
		next_remind_state();
		break;
	case NOTIFY_PHASE:
		next_notify_state();
		break;
	case WARN_PHASE:
		next_warn_state();
		break;
	case WARN_REPEAT_PHASE:
		/* Last phase with no next state */
		/* Index is used as a counter of current warning count */
		state.index++;
		/* FIXME: delete */
		if (state.index > 3)
			exit_program = true;
		break;
	default:
		exit_program = true;
		fputs("Error: next_state(): unknown state\n", stderr);
		notify_error();
	}
}

static void notify_remind_state(void)
{
	notify_time_left(config.reminds[state.index]);
}

static void notify_notify_state(void)
{
	notify_sleeptime();
}

static void notify_warn_state(void)
{
	long tmp = lround(difftime(state.wake_time, state.timer_time));
	/* express left_time in minutes */
	unsigned left_time = (unsigned) (tmp / 60);
	left_time -= config.sleep_preparation;

	unsigned min = left_time % 60;
	unsigned hour = left_time / 60;

	notify_sleep_left(hour, min);
}

static void notify_state(void)
{
	switch (state.phase) {
	case REMIND_PHASE:
		notify_remind_state();
		break;
	case NOTIFY_PHASE:
		notify_notify_state();
		break;
	case WARN_PHASE:
	case WARN_REPEAT_PHASE:
		notify_warn_state();
		break;
	default:
		exit_program = true;
		fputs("Error: notify_state(): unknown state\n", stderr);
		notify_error();
	}
}

static void compute_timer_time(void)
{
	state.timer_time = state.sleep_time;

	switch (state.phase) {
	case REMIND_PHASE:
		state.timer_time -= config.reminds[state.index] * 60;
		break;
	case NOTIFY_PHASE:
		break; /* timer_time = sleep_time */
	case WARN_PHASE:
		state.timer_time += config.warnings[state.index] * 60;
		break;
	case WARN_REPEAT_PHASE:
		state.timer_time += config.warnings[config.warning_count - 1]
									* 60;
		state.timer_time += ((long) state.index + 1)
						* config.warning_repeat * 60;
		break;
	default:
		exit_program = true;
		fputs("Error: compute_timer_time(): unknown state\n", stderr);
		notify_error();
	}
}

static bool set_up_wake_time(time_t current_time)
{
	struct tm *tmp = localtime(&current_time);
	if (tmp == NULL)
		return false;

	struct tm broken_time = *tmp;
	broken_time.tm_sec = 0;
	broken_time.tm_min = (int) config.wake_min;
	broken_time.tm_hour = (int) config.wake_hour;
	broken_time.tm_isdst = -1; /* Automatic detection */

	time_t wake_time = mktime(&broken_time);
	if (wake_time == -1)
		return false;

	if (difftime(wake_time, current_time) <= 0) {
		broken_time.tm_mday++; /* mktime will correct it if necessary */
		broken_time.tm_isdst = -1;

		wake_time = mktime(&broken_time);
		if (wake_time == -1)
			return false;
	}

	if (broken_time.tm_wday == 0 || broken_time.tm_wday == 6)
		wake_time += config.weekend_grace * 60;

	state.wake_time = wake_time;
	return true;
}

static bool set_up_phase(time_t current_time)
{
	state.index = 0;

	long diff = lround(difftime(state.sleep_time, current_time));
	long last_fixed_warn = config.warnings[config.warning_count - 1] * 60;

	if (diff >= config.reminds[config.remind_count - 1] * 60) {
		state.phase = REMIND_PHASE;

		while (diff < config.reminds[state.index] * 60)
			state.index++;
	} else if (diff >= 0) {
		state.phase = NOTIFY_PHASE;
	} else if (-diff <= last_fixed_warn) {
		state.phase = WARN_PHASE;
		diff = -diff;

		while (diff > config.warnings[state.index] * 60)
			state.index++;
	} else {
		state.phase = WARN_REPEAT_PHASE;
		diff = -diff;
		diff -= last_fixed_warn;

		long warning_interval = config.warning_repeat * 60;
		state.index = (unsigned long) (diff / warning_interval);

		if (diff % warning_interval == 0)
			state.index--;
	}

	return true;
}

static bool set_up_state(void)
{
	time_t current_time = time(NULL);
	if (current_time == (time_t) -1)
		return false;

	if (!set_up_wake_time(current_time)) {
		fputs("Error: set_up_state(): set_up_wake_time() !\n", stderr);
		return false;
	}

	fprintf(stderr, "wake_time = %s", ctime(&state.wake_time));

	state.sleep_time = state.wake_time;
	state.sleep_time -= ((config.sleep_hour * 60) + config.sleep_min
					+ config.sleep_preparation) * 60;

	fprintf(stderr, "sleep_time = %s", ctime(&state.sleep_time));

	if (!set_up_phase(current_time)) {
		fputs("Error: set_up_state(): set_up_phase() !\n", stderr);
		return false;
	}

	return true;
}

static bool set_up_timer(void)
{
	if (sigemptyset(&timer_sigset) != 0) {
		fprintf(stderr, "Error: sigemptyset(): %s\n", strerror(errno));
		return false;
	}

	if (sigaddset(&timer_sigset, SIGALRM) != 0) {
		fprintf(stderr, "Error: sigaddset(): %s\n", strerror(errno));
		return false;
	}

	if (sigprocmask(SIG_BLOCK, &timer_sigset, NULL) != 0) {
		fprintf(stderr, "Error: sigprocmask(): %s\n", strerror(errno));
		return false;
	}

	if (timer_create(CLOCK_REALTIME, NULL, &timer) != 0) {
		fprintf(stderr, "Error: timer_create(): %s\n",
							strerror(errno));
		return false;
	}

	return true;
}

static bool schedule_timer(timer_t timerid, time_t expire_time)
{
	struct itimerspec itspec = {
		.it_value = {
			.tv_sec = expire_time
		}
	};

	if (timer_settime(timerid, TIMER_ABSTIME, &itspec, NULL) != 0) {
		fprintf(stderr, "Error: timer_settime(): %s\n",
							strerror(errno));
		return false;
	}

	return true;
}

int main(void)
{
	/* FIXME: handle TERM signal to exit gracefully */
	if (!load_config()) {
		fputs("Error: load_config()\n", stderr);
		return EXIT_FAILURE;
	}

	print_config();
	if (fflush(stdout) == EOF) {
		fputs("Warning: unable to flush stdout, some logs will be missing !", stderr);
	}

	int ret = EXIT_SUCCESS;

	if (!set_up_state()) {
		ret = EXIT_FAILURE;
		goto exit_unload_config;
	}

	if (!set_up_timer()) {
		ret = EXIT_FAILURE;
		goto exit_unload_config;
	}

	if (!notify_init(APP_NAME)) {
		fputs("Error: notify_init()\n", stderr);
		ret = EXIT_FAILURE;
		goto exit_delete_timer;
	}

	while (!exit_program) {
		compute_timer_time();
		fprintf(stderr, "timer_time = %s", ctime(&state.timer_time));

		if (!schedule_timer(timer, state.timer_time)) {
			exit_program = true;
			continue;
		}

		int signum = sigwaitinfo(&timer_sigset, NULL);
		if (signum == -1) {
			if (errno != EINTR) {
				fputs("Error: sigwaitinfo() !\n", stderr);
				exit_program = true;
			}

			continue;
		}

		notify_state();
		next_state();
	}

	notify_uninit();
exit_delete_timer:
	if (timer_delete(timer) != 0) {
		fprintf(stderr, "Warning: timer_delete(): %s\n",
							strerror(errno));
	}
exit_unload_config:
	unload_config();
	return ret;
}
