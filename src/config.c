/*
 * Copyright © 2022 Maël A  < mael dot a at tutanota dot com >
 *
 *
 * This file is part of Nighttime.
 *
 * Nighttime is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Nighttime is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Nighttime. If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <basedir_fs.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))


struct config config;


static bool check_config(void)
{
	bool ok = true;

	if (config.wake_hour > 23 || config.wake_min > 59) {
		fputs("Error: incorrect config wake time !\n", stderr);
		ok = false;
	}

	if (config.sleep_hour == 0 && config.sleep_min == 0) {
		fputs("Error: sleep-hour and sleep-min both set to 0. You do not need Nighttime if you doesn't sleep.\n", stderr);
		ok = false;
	} else if (config.sleep_hour > 23 || config.sleep_min > 59) {
		fputs("Error: sleep-hour and sleep-min are invalid. There is only 24 hours of 60 minutes each in a day.\n", stderr);
		ok = false;
	}

	if (config.remind_count == 0) {
		fputs("Error: at least one remind must be set.\n", stderr);
		ok = false;
	}

	bool decreasing = true;
	for (size_t i = 1; i < config.remind_count; i++) {
		decreasing = decreasing && config.reminds[i - 1] >
							config.reminds[i];
	}

	if (!decreasing) {
		fputs("Error: reminds time values must be decreasing !\n",
								stderr);
		ok = false;
	}

	if (config.warning_count == 0) {
		fputs("Error: at least one warning must be set.\n", stderr);
		ok = false;
	}

	bool increasing = true;
	for (size_t i = 1; i < config.warning_count; i++) {
		increasing = increasing && config.warnings[i - 1] <
							config.warnings[i];
	}

	if (!increasing) {
		fputs("Error: warnings time values must be increasing !\n",
								stderr);
		ok = false;
	}

	if (config.warning_repeat == 0) {
		fputs("Error: warning-repeat must not be set to 0.\n", stderr);
		ok = false;
	}

	return ok;
}

struct config_setter {
	const char *key;
	unsigned *value_store;
	bool (*set_config)(const void *data, unsigned value);
	const void *func_data;
};

struct list_data {
	size_t *length;
	unsigned **ptr;
};

static bool list_append_value(const void *data, unsigned value)
{
	const struct list_data *list = data;

	size_t new_length = *list->length + 1;

	unsigned *new_ptr = realloc(*list->ptr, sizeof(unsigned [new_length]));
	if (new_ptr == NULL) {
		fputs("Error: realloc(): unable to allocate memory !\n",stderr);
		return false;
	}

	*list->ptr = new_ptr;
	(*list->length)++;
	(*list->ptr)[new_length - 1] = value;

	return true;
}

static const struct config_setter config_setters[] = {
	{
		.key = "wake-hour",
		.value_store = &config.wake_hour
	},
	{
		.key = "wake-min",
		.value_store = &config.wake_min
	},
	{
		.key = "weekend-grace",
		.value_store = &config.weekend_grace
	},
	{
		.key = "sleep-hour",
		.value_store = &config.sleep_hour
	},
	{
		.key = "sleep-min",
		.value_store = &config.sleep_min
	},
	{
		.key = "sleep-preparation",
		.value_store = &config.sleep_preparation
	},
	{
		.key = "remind",
		.set_config = list_append_value,
		.func_data = &(struct list_data) {
			.length = &config.remind_count,
			.ptr = &config.reminds
		}
	},
	{
		.key = "warning",
		.set_config = list_append_value,
		.func_data = &(struct list_data) {
			.length = &config.warning_count,
			.ptr = &config.warnings
		}
	},
	{
		.key = "warning-repeat",
		.value_store = &config.warning_repeat
	}
};

static bool set_config_value(const char *key, unsigned value)
{
	const struct config_setter *setter = NULL;

	for (size_t i = 0; i < ARRAY_SIZE(config_setters); i++) {
		if (strcmp(key, config_setters[i].key) == 0) {
			setter = config_setters + i;
			break;
		}
	}

	if (setter == NULL) {
		fprintf(stderr, "Error: unknown config key: %s\n", key);
		return false;
	}

	if (setter->value_store != NULL) {
		*setter->value_store = value;
		return true;
	}

	return setter->set_config(setter->func_data, value);
}

bool load_config(void)
{
	FILE *conffile = xdgConfigOpen("nighttime/nighttime.conf", "r", NULL);
	if (conffile == NULL) {
		fprintf(stderr, "Error: unable to open configuration file !\n");
		return false;
	}

	char key[18];
	unsigned value;
	bool ret = true;
	while (!feof(conffile)) {
		if (fscanf(conffile, " %17[^ =] = %u ", key, &value) != 2) {
			fputs("Error: fscanf(): wrong config file syntax !\n",
									stderr);
			ret = false;
			break;
		}

		if (!set_config_value(key, value)) {
			ret = false;
			break;
		}
	}

	if (fclose(conffile) == EOF)
		fputs("Warning: fclose(conffile) error !\n", stderr);

	return ret && check_config();
}

void unload_config(void)
{
	free(config.reminds);
	free(config.warnings);
}

static void print_list_data(const char *list_name, const struct list_data *data)
{
	unsigned *list = *data->ptr;

	for (size_t i = 0; i < *data->length; i++)
		printf("%s=%u\n", list_name, list[i]);
}

void print_config(void)
{
	for (size_t i = 0; i < ARRAY_SIZE(config_setters); i++) {
		const struct config_setter *setter = config_setters + i;

		if (setter->value_store != NULL)
			printf("%s=%u\n", setter->key, *setter->value_store);
		else
			print_list_data(setter->key, setter->func_data);
	}
}
